### Piglatin Translator

This program translates words, sentences, or paragraphs into piglatin according
to the rules in the assignment.

# How to run
Run the main method in the `App` class. The input string(s) to be translated are provided
as input arguments. Translated output is printed to `stdout`.

# Struncture
- class `Trnaslator.java` is responsible for all the translating logic.

# Assumptions
- there can be only one punctuation mark (') in a word
- only whitespace, new line characters, commas and periods are allowed to delimit words in the input text
- appended letters (suffixes -ay, -way) are lowercased

# Tests
There are **jUnit** tests for the `Translator` class. The following areas are tested
- basic functionality (examples provided in the project assignment)
- punctuation rules (punctuation stays always in the same index from the end)
- white space rules (the program translates paragraphs or sentences without altering whitespace)

