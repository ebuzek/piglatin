package eu.emanuelbuzek.piglatin;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	private Translator translator = new Translator();
	
	/*
	 * Sanity check - requirements in assignment
	 */
	@Test
	public void basicRequirementsTest() {
		assertEquals("Ellohay", translator.translate("Hello"));
		assertEquals("appleway", translator.translate("apple"));
		assertEquals("stairway", translator.translate("stairway"));
		assertEquals("antca'y", translator.translate("can't"));
		assertEquals("endway.", translator.translate("end."));
		assertEquals("histay-hingtay", translator.translate("this-thing"));
		assertEquals("Eachbay", translator.translate("Beach"));
		assertEquals("CcLoudmay", translator.translate("McCloud"));
	}
	
	/*
	 * Punctuation stays in correct position
	 */
	@Test
	public void punctuationTest() {
		assertEquals("Elloha'y", translator.translate("Hell'o"));
		assertEquals("apple'way", translator.translate("ap'ple"));
		assertEquals("stairw'ay", translator.translate("stairw'ay"));
		assertEquals("antcay'", translator.translate("cant'"));
		assertEquals("'", translator.translate("'"));
		assertEquals("histay'-hingta'y", translator.translate("this'-thin'g"));
		assertEquals("Ea'chbay", translator.translate("'Beach"));
		assertEquals("CcLo'udmay", translator.translate("Mc'Cloud"));
	}
	
	/*
	 * Translate sentences and paragraphs - and preserve whitespace
	 */
	@Test
	public void whitespaceTest() {
		assertEquals("   \n\n   Ellohay   \n\n   ", translator.translate("   \n\n   Hello   \n\n   "));
		assertEquals("   \n\n   Ellohay Ellohay   \n\n   ", translator.translate("   \n\n   Hello Hello   \n\n   "));
		assertEquals("   \n\n   Ellohay. Way.   \n\n   ", translator.translate("   \n\n   Hello. Way.   \n\n   "));
	}

}
