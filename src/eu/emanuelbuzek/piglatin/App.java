package eu.emanuelbuzek.piglatin;

public class App {

	/**
	 * Piglatin translator app
	 * 
	 * Provide the string(s) to be translated as the input args.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args == null) {
			throw new IllegalArgumentException(
					"Provide some string to translate as an argument...");
		}
		
		Translator translator = new Translator();
		for (String s : args) {
			System.out.println(translator.translate(s));
		}
	}

}
