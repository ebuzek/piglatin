package eu.emanuelbuzek.piglatin;

import java.util.HashMap;
import java.util.Map;

public class Translator {

	private final char PUNCTUATION = '\'';
	private final String VOWELS = "aeiou";
	
	/**
	 * Translate a word / sentence / paragraph into piglatin.
	 * 
	 * Sentences or paragraphs are tokenized (split) into individual words.
	 * These words are transformed according to the rules, and translated words
	 * are replaced in the original text. This ensures that paragraph structure
	 * (whitespace, indentation...) is preserved in the translated text.
	 * 
	 * Every distinct word is translated only once, and used to replace all
	 * occurrences.
	 * 
	 * @param s
	 * @return
	 */
	public String translate(String s) {
		// Get individual tokens
		String[] tokens = tokenize(s);
		
		// Transform the words to piglatin
		Map<String, String> dictionary = new HashMap<>();
		for (String token : tokens) {
			if (!dictionary.containsKey(token)) {
				dictionary.put(token, translateWord(token));
			}
		}
		
		// Replace translated words in the original text
		for (Map.Entry<String, String> dictPair : dictionary.entrySet()) {
			s = s.replaceAll(dictPair.getKey(), dictPair.getValue());
		}
		
		return s;
	}
	
	/**
	 * Tokenize a string into words to be translated
	 * 
	 * Split the string on whitespace and hyphen to get
	 * individual tokens that will be transformed to piglatin words.
	 * 
	 * @param s string to be translated
	 * @return the array of tokenized strings
	 */
	private String[] tokenize(String s) {
		return s.split("[\\s\\-\\.,]");
	}
	
	/**
	 * Translate individual word (trimmed) into piglatin.
	 * 
	 * @param token String in the original language (from input)
	 * @return string transformed to piglatin
	 */
	private String translateWord(String token) {
		StringBuffer word = new StringBuffer(token);
		// Preserve punctuation - remember the index, but remove it for now
		int punctuationIndex = getPunctuationIndex(word);
		removePunctuation(word);
		// Preserve case - remember the upperCase bit mask
		boolean[] caseMask = getCaseMask(word);
		toLowerCase(word);
		// Do the actual transformation according to piglatin rules
		transformWord(word);
		// Fix case and punctuation
		fixCase(word, caseMask);
		restorePunctuation(word, punctuationIndex);
		return word.toString();
	}
	
	/*
	 * Helper methods
	 */
	
	private boolean[] getCaseMask(StringBuffer word) {
		boolean[] mask = new boolean[word.length()];
		for (int i = 0; i < word.length(); i++) {
			mask[i] = Character.isUpperCase(word.charAt(i));
		}
		return mask;
	}
	
	private void toLowerCase(StringBuffer word) {
		for (int i = 0; i < word.length(); i++) {
			word.setCharAt(i, Character.toLowerCase(word.charAt(i)));
		}
	}
	
	private int getPunctuationIndex(StringBuffer word) {
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(word.length() - 1 - i) == PUNCTUATION) {
				return i;
			}
		}
		return -1;
	}
	
	private void removePunctuation(StringBuffer word) {
		int deleted = 0;
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == PUNCTUATION) {
				word.deleteCharAt(i - deleted);
				deleted++;
			}
		}
	}
	
	private void fixCase(StringBuffer word, boolean[] caseMask) {
		for (int i = 0; i < caseMask.length; i++) {
			Character c = word.charAt(i);
			if (caseMask[i]) {
				// UpperCase
				word.setCharAt(i, Character.toUpperCase(c));
			} else {
				// LowerCase
				word.setCharAt(i, Character.toLowerCase(c));
			}
		}		
	}
	
	private void restorePunctuation(StringBuffer word, int punctuationIndex) {
		if (punctuationIndex > -1) {
			word.insert(word.length() - punctuationIndex, PUNCTUATION);
		}
	}
	
	private void transformWord(StringBuffer word) {
		if (word.length() == 0) {
			return;
		}
		if (word.indexOf("way", word.length() - 3) > -1) {
			// Words that start with a vowel have the letters “way” added to the end.
			return;
		}
		if (!isVowel(word.charAt(0))) {
			// Words that start with a consonant have their first letter moved 
			// to the end of the word and the letters “ay” added to the end.
			char first = word.charAt(0);
			word.deleteCharAt(0);
			word.append(first);
			word.append("ay");
		} else {
			// Words that start with a vowel have the letters “way” added to the end.
			word.append("way");
		}
		
	}
	
	private boolean isVowel(char c) {
		return VOWELS.indexOf(c) > -1;
	}
	
}
